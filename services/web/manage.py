from datetime import time

from flask.cli import FlaskGroup

from project import app, db
from project.models import User, Passage


cli = FlaskGroup(app)

@cli.command("create_db")
def create_db():
    db.create_all()
    db.session.commit()

@cli.command("seed_db")
def seed_db():
    user1 = User(email="alex.benkoe@hotmail.com", first_name="Alexander", last_name="Benkö")
    user1.set_password("admin")

    db.session.add(user1)
    db.session.commit()

    p1 = Passage(passage_nr=1, destination="Calais", departure="Dover", price=100, duration=time(hour=3))
    p2 = Passage(passage_nr=2, destination="Calais", departure="Dunkirk", price=80, duration=time(hour=2))
    p3 = Passage(passage_nr=3, destination="Dover", departure="Calais", price=100, duration=time(hour=3))
    p4 = Passage(passage_nr=4, destination="Dover", departure="Dunkirk", price=75, duration=time(hour=4))
    p5 = Passage(passage_nr=5, destination="Dunkirk", departure="Calais", price=80, duration=time(hour=2))
    p6 = Passage(passage_nr=6, destination="Dunkirk", departure="Dover", price=75, duration=time(hour=4))
    p7 = Passage(passage_nr=7, destination="Livorno", departure="Olbia", price=65, duration=time(hour=5))
    p8 = Passage(passage_nr=8, destination="Olbia", departure="Livorno", price=65, duration=time(hour=5))

    db.session.add_all([p1, p2, p3, p4, p5, p6, p7, p8])
    db.session.commit()

if __name__ == "__main__":
    cli()