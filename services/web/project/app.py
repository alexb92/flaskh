import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

# App
app = Flask(__name__)
app.config.from_object("project.config.Config")
app.secret_key = os.getenv("SECRET_KEY")

# Database
db = SQLAlchemy(app)

# Login
login = LoginManager()
login.init_app(app)
login.login_view = '/login'
