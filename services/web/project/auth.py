from flask import request, render_template, redirect, session
from flask_login import current_user, login_user, logout_user

from .models import User
from .app import app, db


@app.route('/login', methods=('GET', 'POST'))
def _login():
    if current_user.is_authenticated:
        return redirect('/home')

    if request.method == 'POST':
        email = request.form['email']
        user = User.query.filter_by(email=email).first()

        if user and user.check_password(request.form['password']):
            # Login user automatically
            login_user(user)
            session["_user_data"] = {"user_id": user.id ,"name": user.get_public_info()[0], "email": user.get_public_info()[1]}
            return redirect(request.args.get("next", "/home"))

        elif not user:
            return render_template("login.html", title="Login", fail="Email does not exist!")

        elif user and not user.check_password(request.form['password']):
            return render_template("login.html", title="Login", fail="Password does not match email!")

    return render_template("login.html", title="Login")


@app.route('/register', methods=['POST', 'GET'])
def register():
    if current_user.is_authenticated:
        return redirect('/home')

    if request.method == 'POST':
        # Reading form
        email = request.form['email']
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        password = request.form['password']

        # Validity check
        if User.query.filter_by(email=email).all():
            return render_template('register.html', title="Register", fail="Email already exists!")

        # User creation in database
        user = User(email=email, first_name=firstname, last_name=lastname)
        user.set_password(password)
        db.session.add(user)
        db.session.commit()

        # Login user automatically
        login_user(user)
        session["_user_data"] = {"user_id": user.id ,"name": user.get_public_info()[0], "email": user.get_public_info()[1]}

        return redirect('/home')

    return render_template('register.html', title="Register")


@app.route('/logout')
def logout():
    session.clear()
    logout_user()
    return redirect('/home')