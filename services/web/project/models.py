from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from .app import db, login


class Passage(db.Model):
    __tablename__ = 'passage'

    passage_nr = db.Column(db.Integer, unique=True)
    destination = db.Column(db.String(100), primary_key=True)
    departure = db.Column(db.String(100), primary_key=True)
    price = db.Column(db.Integer)
    duration = db.Column(db.Time)

    def to_dict(self):
        return {"passage_nr": self.passage_nr,
                "destination": self.destination,
                "departure": self.departure,
                "price": self.price,
                "duration": str(self.duration)}

    def __repr__(self):
        return f"Passage Nr {self.passage_nr}: {self.departure} - {self.destination}"


class Booking(db.Model):
    __tablename__ = 'booking'

    booking_nr = db.Column(db.Integer, primary_key=True, autoincrement=False)
    booking_date = db.Column(db.DateTime)
    passage_nr = db.Column(db.Integer, db.ForeignKey("passage.passage_nr"))
    passenger_nr = db.Column(db.Integer, db.ForeignKey("users.id"))
    tickets = db.Column(db.Integer)


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), unique=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    password_hash = db.Column(db.String())

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_public_info(self):
        return f"{self.first_name} {self.last_name}", self.email

    def __repr__(self):
        return f"User: {self.first_name} {self.last_name} ; Email: {self.email}"


@login.user_loader
def load_user(id):
    return User.query.get(int(id))