
function toggleCart() {
    if (document.getElementById("myCart").style.height > "0%") {
        document.getElementById("myCart").style.height = "0%";
        document.getElementById("myCart").style.border = "0px";
    }else {
        document.getElementById("myCart").style.height = "100%";
        document.getElementById("myCart").style.borderWidth = "1px";
        document.getElementById("myCart").style.borderColor = "#656566";
        document.getElementById("myCart").style.borderStyle = "solid";
    }
};
function closeCart() {
    document.getElementById("myCart").style.height = "0%";
    document.getElementById("myCart").style.border = "0px";
};