from pathlib import Path
from datetime import datetime
from random import randint, choice

from flask import request, render_template, redirect, session, jsonify
from flask_login import login_required, current_user

from .app import app, db
from .models import Booking, Passage


@app.route("/")
def start():
    return redirect("/home")


@app.route("/home")
def home():
    if "cart" not in session:
        session["cart"] = []
    session['_flashes'] = []
    return render_template("home.html", title="BOAT CAMPUS WIEN")


@app.route('/check')
def check():
    return jsonify({k: v for k, v in session.items()})


@app.route('/destinations', methods=['GET', 'POST'])
def show_destinations():
    destinations = [{"name": "Calais",
                     "file": "Calais.jpg",
                     "text": """Calais is a city and major ferry port in northern France in the department of Pas-de-Calais,
                     of which it is a sub-prefecture. Although Calais is by far the largest city in Pas-de-Calais,
                    the department's prefecture is its third-largest town of Arras."""},
                    {"name": "Dover",
                     "file": "Dover.jpg",
                     "text": """Dover is a town and major ferry port in Kent, South East England. It faces France 
                     across the Strait of Dover, the narrowest part of the English Channel at 33 kilometres from 
                     Cap Gris Nez in France. It lies south-east of Canterbury and east of Maidstone."""},
                    {"name": "Dunkirk",
                     "file": "Dunkirk.jpg",
                     "text": """Dunkirk is a commune in Nord, a French department in northern France. It lies 
                     10 kilometres from the Belgian border. It has the third-largest French harbour. 
                     The population of the commune at the 2016 census was 91,412."""},
                    {"name": "Livorno",
                     "file": "Livorno.jpg",
                     "text": """Livorno is a port city on the Ligurian Sea on the western coast of Tuscany, Italy. 
                     It is the capital of the Province of Livorno, having a population of 158,493 residents in 
                     December 2017. It is traditionally known in English as Leghorn. During the Renaissance, 
                     Livorno was designed as an "Ideal town"."""},
                    {"name": "Olbia",
                     "file": "Olbia.jpg",
                     "text": """Olbia is a city and commune of 60,346 inhabitants in the Italian insular province of 
                     Sassari in northeastern Sardinia, in the historical region of Gallura."""}]

    if request.method == "POST":
        if "departure" in request.form:
            passage = Passage.query.filter_by(destination=request.form["destination"], departure=request.form["departure"]).first().to_dict()
            passage["tickets"] = int(request.form["tickets"])
            passage["item_id"] = ''.join([str(randint(1,10)) for _ in range(5)])
            passage["total_price"] = passage["price"] * int(request.form["tickets"])

            cart = session["cart"]
            cart.append(passage)
            session["cart"] = cart

            return redirect('/home')

        else:
            if not current_user.is_authenticated:
                return redirect('/login')
            destination = request.form['destination']
            departures = Passage.query.filter_by(destination=destination).all()

            return render_template('booking.html', title="Book your Trip", destination=destination, departures=departures, tickets=[i for i in range(1, 10)])


    return render_template('destinations.html', title="Our Destinations", destinations=destinations)


@app.route('/my-bookings', methods=['GET', 'POST'])
@login_required
def my_bookings():
    bookings = Booking.query\
        .join(Passage, Booking.passage_nr == Passage.passage_nr)\
        .add_columns(Booking.booking_nr, Booking.booking_date, Booking.tickets, Passage.departure, Passage.destination)\
        .filter(Booking.passage_nr == Passage.passage_nr)\
        .filter(Booking.passenger_nr == session['_user_data']['user_id']).all()

    qr_codes = [e.name for e in Path(app.config["MEDIA_FOLDER"]).joinpath("QR").iterdir() if e.is_file()]

    bookings = [{"model": b, "qr": choice(qr_codes)} for b in bookings]

    return render_template('my_bookings.html', title="My Bookings", bookings=bookings)


@app.route('/checkout', methods=['GET', 'POST'])
@login_required
def checkout():

    if request.method == "POST":
        if "deletion" in request.form:
            session["cart"] = [i for i in session["cart"] if i["item_id"] != request.form["deletion"]]

        else:
            bookings = []
            for passage in session["cart"]:
                bookings.append(Booking(booking_nr=randint(10000, 100000),
                                        booking_date=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                        passenger_nr=session["_user_data"]["user_id"],
                                        passage_nr=passage["passage_nr"],
                                        tickets=passage["tickets"]))

            db.session.add_all(bookings)
            db.session.commit()
            session["cart"] = []

    total_sum = sum([i["total_price"] for i in session["cart"]])
    return render_template('checkout.html', title="Checkout", total_sum=total_sum)